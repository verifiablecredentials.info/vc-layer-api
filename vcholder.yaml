# Copyright 2019-2022 Crossword Cybersecurity Plc
openapi: 3.0.0
info:
  title: VC Holder
  description: RPC API For VC Holder
  version: '1.0'
  contact:
    email: ioram.sette@crosswordcybersecurity.com
  license:
    name: Crossword Cybersecurity Plc
    url: https://identiproof.io
    
tags:
  - name: VC Holder
    description: VC Holder

servers:
  - url: "https://vcholder:8443"

paths:
  /v1/register:
    post:
      tags:
        - VC Holder
      summary: Register with Issuer
      description: Register with issuer
      operationId: registerWithIssuer
      requestBody:
        required: true
        description: "VC Issuer and Authentication Credentials"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/VCIssuer"
      responses:
        '201':
          description: Created
        '400':
          description: Badly Formed Request or Aborted
        '401':
          description: Authentication Failure
        '404':
          description: Not Found or No Identity Attributes
        '500':
          description: Internal Server Error or Badly Formed Response or FIDO Error

  /v1/reregister:
    post:
      tags:
        - VC Holder
      summary: ReRegister with Issuer
      description: ReRegister with issuer
      operationId: reRegisterWithIssuer
      requestBody:
        required: true
        description: "VC Issuer"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/VCIssuerReRegister"
      responses:
        '201':
          description: Created
        '400':
          description: Badly Formed Request or Aborted
        '401':
          description: Unauthorised or Registration Required
        '404':
          description: Not Found or Unknown Issuer or No Identity Attributes or Please try again
        '500':
          description: Internal Server Error or Badly Formed Response or System Error or FIDO Error
          
  /v1/getVP:
    post:
      tags:
        - VC Holder
      summary: Request VP
      description: Request VP
      operationId: requestVP
      requestBody:
        required: true
        description: "Request VP"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/serviceProvider"
      responses:
        '201':
          description: Created
          headers:
            Location:
              type: string
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/vpJwt"
        '400':
          description: Badly Formed Request
        '401':
          description: Registration Needed, FIDO Authentication Failure
        '404':
          description: No Matching Policy, Unknown VC Verifier
        '500':
          description: Internal Server Error, FIDO Error

components:
  schemas:
    vpJwt:
      type: object
      properties:
        vpjwt:
          type: string
          example:
            "eyJ1c2VybmFtZSI6ImRnMSIsImF0dHNldCI6W3siQGNvbnRleHQiOlsiaHR0cHM6Ly93d3cudzMub3JnLzIwMTgvY3JlZGVudGlhbHMvdjEiLCJodHRwczovL3d3dy5kdmxhLmdvdi51ay9WQ2NvbnRleHQvdjEiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkRyaXZpbmdMaWNlbnNlQ3JlZGVudGlhbCJdLCJuYW1lIjoiZGFpbmlzIiwic3VybmFtZSI6ImdyaW5iZXJncyIsImRyaXZpbmdMaWNlbnNlIjp7Im5hbWUiOiJkYWluaXMgZ3JpbmJlcmdzIiwiRE9CIjoiMDEvMDEvMjAwMCIsInZlaGljbGVzIjoiY2FyLCBtb3BlZCwgYmlrZSJ9fSx7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIiwiaHR0cHM6Ly93d3cua2VudC5hYy51ay9WQ2NvbnRleHQvdjEiXSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkRlZ3JlZUNyZWRlbnRpYWwiXSwicHJvcDEiOiJ2YWwxIiwicHJvcDIiOiJ2YWwyIiwiZGVncmVlIjp7InR5cGUiOiJCYWNoZWxvckRlZ3JlZSIsIm5hbWUiOiJCYWNoZWxvciBvZiBTY2llbmNlIGFuZCBBcnRzIn19XSwiYXV0aG5DcmVkcyI6eyJ1c2VybmFtZSI6ImRnMSIsIm90cCI6ImFiY2RlZiJ9fQ=="

    vcIssuer:
      type: object
      properties:
        vcIssuer:
          type: string
          example: "https://kent.ac.uk/vcissuer/"
        authnCreds:
          type: object
          example:
            username: "dc1"
            otp: "abcdef"

    vcIssuerReRegister:
      type: object
      properties:
        vcIssuer:
          type: string
          example: "https://kent.ac.uk/vcissuer/"

    serviceProvider:
      type: object
      properties:
        sp:
          type: string
          description: SP's resource redirection URL
          example: "https://www.su.kent.ac.uk/tickets/"
        vcVerifier:
          type: string
          description: URL of SP's VC verifier server
          example: "https://www.su.kent.ac.uk/vcverifier/"
        authnCreds:
          type: object
          example:
            username: "dc1"
            otp: "abcdef"
        policyMatch:
          type: object
          example:
            type: "Over 18"
